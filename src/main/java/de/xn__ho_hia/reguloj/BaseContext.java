/*
 * This file is part of reguloj. It is subject to the license terms in the LICENSE file found in the top-level
 * directory of this distribution and at http://creativecommons.org/publicdomain/zero/1.0/. No part of reguloj,
 * including this file, may be copied, modified, propagated, or distributed except according to the terms contained
 * in the LICENSE file.
 */
package de.xn__ho_hia.reguloj;

/**
 * Basic implementation of the {@link Context} interface.
 *
 * @param <TOPIC>
 *            The topic of the context.
 */
public class BaseContext<TOPIC> implements Context<TOPIC> {

    private final TOPIC topic;

    /**
     * @param topic
     *            The topic of this context.
     */
    public BaseContext(final TOPIC topic) {
        this.topic = topic;
    }

    @Override
    public final TOPIC getTopic() {
        return topic;
    }

}

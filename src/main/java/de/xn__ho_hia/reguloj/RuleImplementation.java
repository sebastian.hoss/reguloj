/*
 * This file is part of reguloj. It is subject to the license terms in the LICENSE file found in the top-level
 * directory of this distribution and at http://creativecommons.org/publicdomain/zero/1.0/. No part of reguloj,
 * including this file, may be copied, modified, propagated, or distributed except according to the terms contained
 * in the LICENSE file.
 */
package de.xn__ho_hia.reguloj;

import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Predicate;

final class RuleImplementation<CONTEXT extends Context<?>> implements Rule<CONTEXT> {

    private final String             name;
    private final Predicate<CONTEXT> predicate;
    private final Consumer<CONTEXT>  consumer;

    RuleImplementation(final String name, final Predicate<CONTEXT> predicate, final Consumer<CONTEXT> consumer) {
        this.name = name;
        this.predicate = predicate;
        this.consumer = consumer;
    }

    @Override
    public void run(final CONTEXT context) {
        if (fires(context)) {
            consumer.accept(context);
        }
    }

    @Override
    public boolean fires(final CONTEXT context) {
        return predicate.test(context);
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, predicate, consumer);
    }

    @Override
    public boolean equals(final Object object) {
        if (object != null && object instanceof RuleImplementation) {
            final RuleImplementation<?> that = (RuleImplementation<?>) object;

            return Objects.equals(name, that.name) && Objects.equals(predicate, that.predicate)
                    && Objects.equals(consumer, that.consumer);
        }

        return false;
    }

}

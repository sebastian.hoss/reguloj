/*
 * This file is part of reguloj. It is subject to the license terms in the LICENSE file found in the top-level
 * directory of this distribution and at http://creativecommons.org/publicdomain/zero/1.0/. No part of reguloj,
 * including this file, may be copied, modified, propagated, or distributed except according to the terms contained
 * in the LICENSE file.
 */
package de.xn__ho_hia.reguloj;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;

import org.junit.Assert;
import org.junit.Test;

/**
 *
 *
 */
@SuppressWarnings("static-method")
public class RuleEnginesTest {

    /**
     * Test method for {@link de.xn__ho_hia.reguloj.RuleEngines#chained()}.
     */
    @Test
    public void shouldCreateChainedEngine() {
        Assert.assertNotNull(RuleEngines.chained());
    }

    /**
     * Test method for {@link de.xn__ho_hia.reguloj.RuleEngines#limited(int)}.
     */
    @Test
    public void shouldCreateLimitedEngine() {
        Assert.assertNotNull(RuleEngines.limited(1));
    }

    /**
     * Test method for {@link de.xn__ho_hia.reguloj.RuleEngines#firstWins()}.
     */
    @Test
    public void shouldCreateFirstWinsEngine() {
        Assert.assertNotNull(RuleEngines.firstWins());
    }

    /**
     * @throws NoSuchMethodException
     *             Reflection problem.
     * @throws IllegalAccessException
     *             Reflection problem.
     * @throws InvocationTargetException
     *             Reflection problem.
     * @throws InstantiationException
     *             Reflection problem.
     */
    @Test
    public void shouldDeclarePrivateConstructor()
            throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        // Given
        final Constructor<RuleEngines> constructor = RuleEngines.class.getDeclaredConstructor();

        // When
        final boolean isPrivate = Modifier.isPrivate(constructor.getModifiers());

        // Then
        Assert.assertTrue("Constructor is not private", isPrivate); //$NON-NLS-1$
        constructor.setAccessible(true);
        constructor.newInstance();
    }

}

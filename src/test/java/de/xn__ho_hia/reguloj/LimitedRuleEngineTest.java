/*
 * This file is part of reguloj. It is subject to the license terms in the LICENSE file found in the top-level
 * directory of this distribution and at http://creativecommons.org/publicdomain/zero/1.0/. No part of reguloj,
 * including this file, may be copied, modified, propagated, or distributed except according to the terms contained
 * in the LICENSE file.
 */
package de.xn__ho_hia.reguloj;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;

import de.xn__ho_hia.utils.jdt.CompilerWarnings;

/**
 * Test case for LimitedRuleEngine.
 */
@SuppressWarnings({ CompilerWarnings.BOXING, CompilerWarnings.UNCHECKED })
public class LimitedRuleEngineTest {

    private RuleEngine<Context<Object>> engine;
    private Context<Object>             context;
    private Rule<Context<Object>>       rule1;
    private Rule<Context<Object>>       rule2;

    /**
     * Creates rule engine, context and rules.
     */
    @Before
    public void setup() {
        engine = new LimitedRuleEngine<>(2);
        context = Mockito.mock(Context.class);
        rule1 = Mockito.mock(Rule.class);
        rule2 = Mockito.mock(Rule.class);
    }

    /**
     * Ensures that rules will be called a maximum of two times (as specified in {@link #setup()} with matching rules.
     */
    @Test
    public void shouldRunTwoTimesWithMatchingRules() {
        BDDMockito.given(rule1.fires(context)).willReturn(Boolean.TRUE);
        BDDMockito.given(rule2.fires(context)).willReturn(Boolean.TRUE);

        engine.infer(rules(), context);

        Mockito.verify(rule1, Mockito.times(2)).run(context);
        Mockito.verify(rule2, Mockito.times(2)).run(context);
    }

    /**
     * Ensures that all rules will be only called once, if there are no matching rules.
     */
    @Test
    public void shouldRunOnceWithNonMatchingRules() {
        BDDMockito.given(rule1.fires(context)).willReturn(Boolean.FALSE);
        BDDMockito.given(rule2.fires(context)).willReturn(Boolean.FALSE);

        engine.infer(rules(), context);

        Mockito.verify(rule1, Mockito.times(1)).fires(context);
        Mockito.verify(rule2, Mockito.times(1)).fires(context);
    }

    private Collection<Rule<Context<Object>>> rules() {
        final Collection<Rule<Context<Object>>> rules = new ArrayList<>();
        rules.add(rule1);
        rules.add(rule2);
        return Collections.unmodifiableCollection(rules);
    }

}

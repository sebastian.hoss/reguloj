/*
 * This file is part of reguloj. It is subject to the license terms in the LICENSE file found in the top-level
 * directory of this distribution and at http://creativecommons.org/publicdomain/zero/1.0/. No part of reguloj,
 * including this file, may be copied, modified, propagated, or distributed except according to the terms contained
 * in the LICENSE file.
 */
package de.xn__ho_hia.reguloj;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import com.google.common.truth.Truth;

import org.junit.Before;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.BDDMockito;
import org.mockito.Mockito;

import de.xn__ho_hia.utils.jdt.CompilerWarnings;

/**
 * Test cases for the ChainedRuleEngine.
 */
@SuppressWarnings({ CompilerWarnings.BOXING, CompilerWarnings.UNCHECKED })
public class ChainedRuleEngineTest {

    /** Checks expected exception inside single test cases. */
    @org.junit.Rule
    public ExpectedException            thrown = ExpectedException.none();

    private RuleEngine<Context<Object>> engine;
    private Context<Object>             context;
    private Rule<Context<Object>>       rule;

    /**
     * Creates rule engine and context.
     */
    @Before
    public void setup() {
        engine = new ChainedRuleEngine<>();
        context = Mockito.mock(Context.class);
        rule = Mockito.mock(Rule.class);
    }

    /**
     * <p>
     * Test method for ChainedRuleEngine#analyze(Context, java.util.Set)
     * </p>
     * <p>
     * Ensures that <code>false</code> is returned when passing in an empty set.
     * </p>
     */
    @Test
    public void shouldReturnFalseForEmptyRuleSet() {
        final Set<Rule<Context<Object>>> rules = new HashSet<>();

        final boolean fired = engine.analyze(rules, context);

        Truth.assertThat(fired).isFalse();
    }

    /**
     * <p>
     * Test method for ChainedRuleEngine#analyze(Context, Set)
     * </p>
     * <p>
     * Ensures that <code>true</code> is returned if any rule can fire.
     * </p>
     */
    @Test
    public void shouldReturnTrueIfRuleFired() {
        BDDMockito.given(rule.fires(context)).willReturn(Boolean.TRUE);

        final boolean fired = engine.analyze(rules(), context);

        Truth.assertThat(fired).isTrue();
    }

    /**
     * <p>
     * Test method for ChainedRuleEngine#analyze(Context, Set)
     * </p>
     * <p>
     * Ensures that <code>false</code> is returned if no rule can fire.
     * </p>
     */
    @Test
    public void shouldReturnFalseIfNoRuleFires() {
        BDDMockito.given(rule.fires(context)).willReturn(Boolean.FALSE);

        final boolean fired = engine.analyze(rules(), context);

        Truth.assertThat(fired).isFalse();
    }

    /**
     * <p>
     * Test method for ChainedRuleEngine#infer(Context, Set)
     * </p>
     * <p>
     * Ensures that the engine can handle an empty rule set.
     * </p>
     */
    @Test
    public void shouldRunWithEmptyRuleSet() {
        final Set<Rule<Context<Object>>> rules = new HashSet<>();

        engine.infer(rules, context);
    }

    /**
     * <p>
     * Test method for ChainedRuleEngine#infer(Context, Set)
     * </p>
     * <p>
     * Ensures that the engine loops if any rule can fire.
     * </p>
     */
    @Test
    public void shouldLoopWithFiringRule() {
        BDDMockito.given(rule.fires(context)).willReturn(Boolean.TRUE).willReturn(Boolean.FALSE);

        engine.infer(rules(), context);

        Mockito.verify(rule, Mockito.times(2)).fires(context);
        Mockito.verify(rule, Mockito.times(1)).run(context);
    }

    /**
     * <p>
     * Test method for ChainedRuleEngine#infer(Context, Set)
     * </p>
     * <p>
     * Ensures that the engine does not loop if no rule can fire.
     * </p>
     */
    @Test
    public void shouldNotLoopWithNotFiringRule() {
        BDDMockito.given(rule.fires(context)).willReturn(Boolean.FALSE);

        engine.infer(rules(), context);

        Mockito.verify(rule, Mockito.times(1)).fires(context);
        Mockito.verify(rule, Mockito.times(0)).run(context);
    }

    private Collection<Rule<Context<Object>>> rules() {
        final Collection<Rule<Context<Object>>> rules = new ArrayList<>();
        rules.add(rule);
        return Collections.unmodifiableCollection(rules);
    }

}

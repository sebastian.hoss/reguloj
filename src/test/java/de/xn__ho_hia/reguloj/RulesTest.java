/*
 * This file is part of reguloj. It is subject to the license terms in the LICENSE file found in the top-level
 * directory of this distribution and at http://creativecommons.org/publicdomain/zero/1.0/. No part of reguloj,
 * including this file, may be copied, modified, propagated, or distributed except according to the terms contained
 * in the LICENSE file.
 */
package de.xn__ho_hia.reguloj;

import java.lang.reflect.Constructor;
import java.util.function.Consumer;
import java.util.function.Predicate;

import com.google.common.truth.Truth;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import de.xn__ho_hia.utils.jdt.CompilerWarnings;

/**
 * Test cases for the {@link Rules} utility class.
 *
 * @see Rules
 */
@SuppressWarnings({ CompilerWarnings.STATIC_METHOD })
public final class RulesTest {

    /** Constant name for all rules inside this test. */
    private static final String NAME = "test rule"; //$NON-NLS-1$

    /**
     * <p>
     * Test method for {@link Rules#rule()}
     * </p>
     * <p>
     * Ensures that the returned object is not <code>null</code>.
     * </p>
     */
    @Test
    public void shouldCreateBuilder() {
        RuleBuilder<Context<Object>> builder;

        builder = Rules.<Context<Object>> rule();

        Truth.assertThat(builder).isNotNull();
    }

    /**
     * <p>
     * Test method for {@link Rules#rule()}
     * </p>
     * <p>
     * Ensures that the returned object is not <code>null</code>.
     * </p>
     */
    @Test
    @SuppressWarnings("unchecked")
    public void shouldCreateRule() {
        final RuleBuilder<Context<Object>> builder = Rules.<Context<Object>> rule();
        builder.called(RulesTest.NAME).when(Mockito.mock(Predicate.class));

        final Rule<Context<Object>> rule = builder.then(Mockito.mock(Consumer.class));

        Truth.assertThat(rule).isNotNull();
    }

    /**
     * <p>
     * Test method for {@link Rules Rules()}
     * </p>
     * <p>
     * Ensures that the constructor is not accessible from the outside.
     * </p>
     */
    @Test
    @SuppressWarnings("boxing")
    public void shouldNotBeInvocable() {
        final Class<?> clazz = Rules.class;

        final Constructor<?>[] constructors = clazz.getDeclaredConstructors();

        for (final Constructor<?> constructor : constructors) {
            Truth.assertThat(constructor.isAccessible()).isFalse();
        }
    }

    /**
     * <p>
     * Test method for {@link Rules Rules()}
     * </p>
     * <p>
     * Ensures that the constructor is accessible via reflection.
     * </p>
     *
     * @throws Exception
     *             When no new instance can be created.
     */
    @Test
    public void shouldBeInvocableViaReflection() throws Exception {
        final Class<?> clazz = Rules.class;
        final Constructor<?> constructor = clazz.getDeclaredConstructors()[0];

        constructor.setAccessible(true);
        final Object instance = constructor.newInstance((Object[]) null);

        Assert.assertNotNull(instance);
    }

}

/*
 * This file is part of reguloj. It is subject to the license terms in the LICENSE file found in the top-level
 * directory of this distribution and at http://creativecommons.org/publicdomain/zero/1.0/. No part of reguloj,
 * including this file, may be copied, modified, propagated, or distributed except according to the terms contained
 * in the LICENSE file.
 */
package de.xn__ho_hia.reguloj;

import java.util.function.Consumer;
import java.util.function.Predicate;

import com.google.common.truth.Truth;

import org.junit.Test;
import org.mockito.Mockito;

import de.xn__ho_hia.utils.jdt.CompilerWarnings;

/**
 * Test cases for the RuleBuilderImplementation.
 */
@SuppressWarnings({ CompilerWarnings.STATIC_METHOD, CompilerWarnings.UNCHECKED })
public final class RuleBuilderImplementationTest {

    /**
     * <p>
     * Test method for RuleBuilderImplementation#then(Conclusion)
     * </p>
     * <p>
     * Ensures that rules can be created with a valid RuleBuilderImplementation.
     * </p>
     */
    @Test
    public void shouldCreateRuleIfAllValuesAreSet() {
        final RuleBuilder<Context<Object>> builder = new RuleBuilderImplementation<>();

        final Rule<Context<Object>> rule = builder.called("test rule") //$NON-NLS-1$
                .when(Mockito.mock(Predicate.class))
                .then(Mockito.mock(Consumer.class));

        Truth.assertThat(rule).isNotNull();
    }

    /**
     * <p>
     * Test method for RuleBuilderImplementation#then(Conclusion)
     * </p>
     * <p>
     * Ensures that rules can be created with a valid RuleBuilderImplementation.
     * </p>
     */
    @Test
    public void shouldReturnBuilderAfterPredicate() {
        final RuleBuilder<Context<Object>> builder = new RuleBuilderImplementation<>();

        final RuleBuilder<Context<Object>> ruleBuilder = builder.called("test rule") //$NON-NLS-1$
                .when(Mockito.mock(Predicate.class));

        Truth.assertThat(ruleBuilder).isNotNull();
    }

}

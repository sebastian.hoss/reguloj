/*
 * This file is part of reguloj. It is subject to the license terms in the LICENSE file found in the top-level
 * directory of this distribution and at http://creativecommons.org/publicdomain/zero/1.0/. No part of reguloj,
 * including this file, may be copied, modified, propagated, or distributed except according to the terms contained
 * in the LICENSE file.
 */
package de.xn__ho_hia.reguloj;

import com.google.common.truth.Truth;

import org.junit.Test;

import de.xn__ho_hia.utils.jdt.CompilerWarnings;

/**
 * Test cases for {@link BaseContext}.
 */
@SuppressWarnings({ CompilerWarnings.NLS, CompilerWarnings.STATIC_METHOD })
public class BaseContextTest {

    /**
     * Ensures that the given topic is returned.
     */
    @Test
    public void shouldReturnGivenTopic() {
        final Context<Object> context = new BaseContext<>("test");

        final Object topic = context.getTopic();

        Truth.assertThat(topic).isEqualTo("test");
    }

}
